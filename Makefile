GO:=go


all: unruly

unruly: $(shell find . -iname '*.go')
	go build -o $@ ./cmd/$@
TO_CLEAN+=unruly

.PHONY: test
test: unruly
	@echo "== go test =="
	@$(GO) test ./...
	@echo "\n== diff test =="
	@./test/test.sh

.PHONY: clean
clean:
	rm -rf $(TO_CLEAN)