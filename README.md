# unruly
`brule` is an experimental extension to [Semgrep rule syntax](https://semgrep.dev/docs/writing-rules/rule-syntax/)
that addresses composability and simplified logic.


## `brule` file syntax

`brule` is `Semgrep` with a few extra keys, `define`, `include`, and `pattern-brule`.

### `include`
External `brule` files can be "included" using the document key `include`:
```yaml
include:
  - /rules/brule/lib/brule_lib1.yml

  # paths are relative to including file.
  - brule_lib2.yml 
...
```

### `define`
`brule` "functions" are defined using the document key `define`:
```yaml
define:
  - name: in_main

    # each function must have one and only one pattern, pattern-*, or metavariable-* key
    pattern-inside: |
      def main(...):
        ...

  - name: transforms_something
    # params are optional
    #  when a function is called with metavariable params, they're substituted.
    params: [$Y, $X]

    # non-param metavariables are "leaked" to calling scope
    pattern: |
      $X = $O.Transform($Y)

  - name: transforms_something_in_main
    params: [$A]

    # brule functions can use a brule pattern as long as it don't recurse
    pattern-brule: in_main and transforms_something($A, $A)
    
    # e.g. the following would produce an error
    # pattern-brule: transforms_something_in_main($A)
```

### `pattern-brule`
In a `brule` file, `pattern-brule` can be used anywhere that a `pattern`, `pattern-*`, or `metavariable-*` can be used:

```yaml
rules:
  - id: somerule
    languages: [python]
    message: Found $V
    severity: "UNKNOWN"
    
    pattern-either:
      - pattern-brule: transforms_something_in_main($V)
      - pattern: $V.Transform()
```

which, along with the definitions above, expands to the following Semgrep:

```yaml
rules:
  - id: somerule
    languages: [python]
    message: Found $V
    severity: "UNKNOWN"
    
    pattern-either:
      - patterns:
        - pattern-inside: |
            def main(...):
              ...
        - pattern: |
            $V = $O.Transform($V)
      - pattern: $V.Transform()
```



#### `brule` pattern syntax
`pattern-brule` values are boolean combinations of functions calls.

In [BNF](https://www.w3.org/Notation.html):
```
rules:
  <brule>   ::= <or> <END>
  <or>      ::= <where> *("or" <where>)
  <where>   ::= <and> ["where" <wclause> *("," <wclause>)]
  <wclause> ::= <META> ("has" "type" <ID> | "matches" <and>)
  <and>     ::= <pred> *("and" <pred>)
  <pred>    ::= <not> | <atom>
  <not>     ::= "not" <atom>
  
  <atom>    ::= <REGEX> | <fun> | "(" <brule> ")"
  <fun>     ::= <ID> [<args>]
  <args>    :: "(" <arg> *("," <arg>) ")"
  <arg>     ::= <META> | <fun>

tokens:
  <REGEX> ~ "/" delimited regular expressions
  <ID>    ~ identifiers starting with a-z, A-Z, or _
  <META>  ~ identifiers starting with $
```

## tools

### `unruly`
The command line tool `unruly` is used to convert to and from Semgrep:
```sh
unruly brule2semgrep brule.yml > semgrep.yml
```

When multiple input files are given, the output will include multiple YAML
documents separated by `---`.