package brule

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/gitlab-org/secure/tools/unruly/parser"
	"gitlab.com/gitlab-org/secure/tools/unruly/semgrep"
	"gitlab.com/gitlab-org/secure/tools/unruly/util"
	"gopkg.in/yaml.v3"
)

type Function struct {
	Name   string   `yaml:"name"`
	Params []string `yaml:"params"`

	Body *yaml.Node `yaml:",remaining"`
}

type FunctionResolver interface {
	Resolve(string) (*Function, bool)
}

type mapFunctionResolver struct {
	m map[string]*Function
}

func newMapFunctionResolver() *mapFunctionResolver {
	return &mapFunctionResolver{m: make(map[string]*Function)}
}

func (mfr *mapFunctionResolver) Resolve(name string) (*Function, bool) {
	f, ok := mfr.m[name]
	return f, ok
}

func (mfr *mapFunctionResolver) add(name string, f *Function) {
	mfr.m[name] = f
}

// LayeredFunctionResolver searches resolves each name using a slice of resolvers
type LayeredFunctionResolver struct {
	resolvers []FunctionResolver
}

func NewLayeredFunctionResolver() *LayeredFunctionResolver {
	return &LayeredFunctionResolver{resolvers: make([]FunctionResolver, 0)}
}

func (sfr *LayeredFunctionResolver) Resolve(name string) (*Function, bool) {
	for _, r := range sfr.resolvers {
		f, ok := r.Resolve(name)
		if ok {
			return f, ok
		}
	}
	return nil, false
}

// Add another, lower priority, resolver
func (sfr *LayeredFunctionResolver) AddResolver(r FunctionResolver) {
	if r != nil {
		sfr.resolvers = append(sfr.resolvers, r)
	}
}

type BruleFile struct {
	LeadComment string
	Path        string
	Functions   FunctionResolver
	Includes    []string
	root        yaml.Node
}

func NewBruleFile(path string) (*BruleFile, error) {
	brules := &BruleFile{Path: path}

	return brules, brules.load()
}

func (bf *BruleFile) load() error {
	data, err := os.ReadFile(bf.Path)
	if err != nil {
		return err
	}

	bf.LeadComment, data = util.SplitLeadComment(data)

	if err := yaml.Unmarshal(data, &bf.root); err != nil {
		return err
	}

	// parse definitions into bf.Definitions
	if bf.root.Kind != yaml.DocumentNode {
		return fmt.Errorf("top level element of brule file must be a document node: %+v", bf.root)
	}
	root := bf.root.Content[0]
	if root.Kind != yaml.MappingNode {
		return fmt.Errorf("brule file document entry must be a mapping: %+v", root)
	}

	// parse special root keys
	for i := 0; i < len(root.Content)-1; i += 2 {
		kn, vn := root.Content[i], root.Content[i+1]

		if kn.Value == "define" {
			if vn.Kind != yaml.SequenceNode {
				return fmt.Errorf("brule file .define must be a sequence: %+v", vn)
			}
			bf.Functions, err = LoadFunctions(vn.Content)
			if err != nil {
				return err
			}
		} else if kn.Value == "include" {
			if vn.Kind != yaml.SequenceNode {
				return fmt.Errorf("brule file .include must be a sequence: %+v", vn)
			}
			bf.Includes = make([]string, len(vn.Content))
			dn := filepath.Dir(bf.Path)
			for i, c := range vn.Content {
				if c.Kind != yaml.ScalarNode {
					return fmt.Errorf("brule file .include entry must be a scalar: %+v", c)
				}
				p := c.Value
				if !filepath.IsAbs(p) {
					p = filepath.Join(dn, p)
				}

				bf.Includes[i], err = filepath.Abs(p)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func LoadFunctions(definitions []*yaml.Node) (*mapFunctionResolver, error) {
	functions := newMapFunctionResolver()
	for _, defineNode := range definitions {
		if defineNode.Kind != yaml.MappingNode {
			return nil, fmt.Errorf("brule file .define sequence entries must be mappings: %+v", defineNode)
		}
		var define Function
		if err := util.UnmarshalWithRemaining(defineNode, &define); err != nil {
			return nil, err
		}
		functions.add(define.Name, &define)
	}

	return functions, nil
}

// apply transforms, left to right
func chaincall(n *yaml.Node, l ...func(*yaml.Node) (*yaml.Node, error)) (*yaml.Node, error) {
	var err error
	for _, f := range l {
		n, err = f(n)
		if err != nil {
			return nil, err
		}
	}
	return n, nil
}

func (bf *BruleFile) ToSemgrep(functions FunctionResolver) (*yaml.Node, error) {
	b := semgrepBuilder{functions: functions}
	n, err := b.buildFrom(ROOT_PARENT, &bf.root)
	if err != nil {
		return nil, err
	}

	return chaincall(n, semgrep.Nnf, semgrep.Flatten, semgrep.FixMetavariablePatterns)
}

type Walker struct {
	depth int
}

func (w *Walker) Recurse(n any) error {
	w.depth += 1
	defer func() { w.depth -= 1 }()
	ind := strings.Repeat("  ", w.depth-1)

	if m, ok := n.(map[string]yaml.Node); ok {
		fmt.Printf("%smap:\n", ind)
		for k, v := range m {
			fmt.Printf("%s  %s:\n", ind, k)
			w.depth += 1
			w.Recurse(&v)
			w.depth -= 1
		}
	} else if node, ok := n.(*yaml.Node); ok {
		switch node.Kind {
		case yaml.ScalarNode:
			fmt.Printf("%sScalar Node: %s\n", ind, node.Value)
		case yaml.MappingNode:
			fmt.Printf("%sMapping Node (Map):\n", ind)
			for i := 0; i < len(node.Content); i += 2 {
				key := node.Content[i]
				if key.Kind != yaml.ScalarNode {
					return fmt.Errorf("map key is not a scalar: %+v", key)
				}
				value := node.Content[i+1]
				fmt.Printf("%s  %s:\n", ind, key.Value)
				w.depth += 1
				if err := w.Recurse(value); err != nil {
					return err
				}
				w.depth -= 1
			}
		case yaml.SequenceNode:
			fmt.Printf("%sSequence Node (List):\n", ind)
			for i, item := range node.Content {
				fmt.Printf("%s  [%d]\n", ind, i)
				w.depth += 1
				if err := w.Recurse(item); err != nil {
					return err
				}
				w.depth -= 1
			}
		default:
			fmt.Printf("Unknown Node Type: %+v\n", node)
		}
	} else {
		fmt.Printf("Unrecognized argument to Recurse: %+v\n", n)
	}
	return nil
}

type substitution struct {
	p *regexp.Regexp
	r string
}

func (sub *substitution) apply(s string) string {
	return sub.p.ReplaceAllLiteralString(s, sub.r)
}

type semgrepBuilder struct {
	functions FunctionResolver
	fresh     int

	// substitution stack
	subs []*substitution

	// visitation stack to prevent infinite recursions in brule function expansion
	stack []*yaml.Node
}

func (b *semgrepBuilder) freshMeta() *parser.Named {
	n := b.fresh
	b.fresh++
	return &parser.Named{
		Name: parser.NodeName_Meta,
		Parts: []any{
			fmt.Sprintf("$_%d", n),
		},
	}
}

// rewrite brule parsetree in place, expanding sugar
func (b *semgrepBuilder) preprocessBrule(bt any) error {
	switch n := bt.(type) {
	case *parser.Named:
		// preorder replacement
		// f(a) -> f($fresh) where $fresh matches a
		if n.Name == parser.NodeName_Fun {
			if len(n.Parts) > 1 {
				args := n.Parts[1].(*parser.Named).Parts
				whereParts := make([]any, 1)
				for i, arg := range args {
					if arg.(*parser.Named).Name != parser.NodeName_Meta {
						mv := b.freshMeta()
						whereParts = append(whereParts, mv, arg)
						args[i] = mv
					}
				}

				if len(whereParts) > 1 {
					// change the parsetree in place, by making n a Where node
					// and creating a new Fun node as its expression
					whereParts[0] = &parser.Named{Name: parser.NodeName_Fun, Parts: n.Parts}
					n.Name = parser.NodeName_Where
					n.Parts = whereParts
				}
			}
		}

		for _, c := range n.Parts {
			b.preprocessBrule(c)
		}
	}
	return nil
}

func (b *semgrepBuilder) parseBrule(vn *yaml.Node) (*parser.Named, error) {
	if vn.Kind != yaml.ScalarNode {
		return nil, fmt.Errorf("pattern-brule must be a string: %+v", vn)
	}
	bt, err := parser.Parse(vn.Value)
	if err != nil {
		return nil, fmt.Errorf("failed to parse `%s`: %v", vn.Value, err)
	}
	err = b.preprocessBrule(bt)
	if err != nil {
		return nil, err
	}
	return bt, nil
}

// append a key or keys to n corresponding to bt.Name and recurse
func (b *semgrepBuilder) appendBrule(n *yaml.Node, bt *parser.Named) error {
	switch bt.Name {
	case parser.NodeName_Not:
		nn := util.Ymap()
		util.YmapSet(n, "pattern-not", nn)
		return b.appendBrule(nn, bt.Parts[0].(*parser.Named))
	case parser.NodeName_And:
		nn := util.Yseq()
		util.YmapSet(n, "patterns", nn)
		for _, cbt := range bt.Parts {
			nnn := util.Ymap()
			nn.Content = append(nn.Content, nnn)
			err := b.appendBrule(nnn, cbt.(*parser.Named))
			if err != nil {
				return err
			}
		}
	case parser.NodeName_Where:
		if len(bt.Parts) < 3 || (len(bt.Parts)-1)%2 != 0 {
			return fmt.Errorf("Where node must have an odd number parts >= 3: %+v", bt.Parts)
		}

		expr := bt.Parts[0]
		patterns := util.Yseq()
		util.YmapSet(n, "patterns", patterns)

		// parse and add the expression to patterns
		exprPattern := util.Ymap()
		patterns.Content = append(patterns.Content, exprPattern)
		err := b.appendBrule(exprPattern, expr.(*parser.Named))
		if err != nil {
			return err
		}

		for i := 1; i < len(bt.Parts)-1; i += 2 {
			mv, clause := bt.Parts[i], bt.Parts[i+1]
			mvc, err := parser.TryNamed(mv, parser.NodeName_Meta)
			if err != nil {
				return err
			}
			mvName := mvc[0].(string)

			// parse the where clause, create a matcher, and append it
			metaPattern := util.Ymap()
			patterns.Content = append(patterns.Content, metaPattern)

			clausePattern := util.Ymap()
			util.YmapSet(clausePattern, "metavariable", util.Ystr(mvName))

			c, err := parser.TryNamed(clause, parser.NodeName_Type)
			if err == nil {
				util.YmapSet(metaPattern, "metavariable-type", clausePattern)
				util.YmapSet(clausePattern, "type", util.Ystr(c[0].(string)))
			} else {
				c, err = parser.TryNamed(clause, parser.NodeName_Regex)
				if err == nil {
					util.YmapSet(metaPattern, "metavariable-regex", clausePattern)
					re := c[0].(string)
					util.YmapSet(clausePattern, "regex", util.Ystr(re[1:len(re)-1]))
				} else {
					util.YmapSet(metaPattern, "metavariable-pattern", clausePattern)
					err = b.appendBrule(clausePattern, clause.(*parser.Named))
					if err != nil {
						return err
					}
				}
			}
		}

	case parser.NodeName_Or:
		nn := util.Yseq()
		util.YmapSet(n, "pattern-either", nn)
		for _, cbt := range bt.Parts {
			nnn := util.Ymap()
			nn.Content = append(nn.Content, nnn)
			err := b.appendBrule(nnn, cbt.(*parser.Named))
			if err != nil {
				return err
			}
		}
	case parser.NodeName_Regex:
		re := bt.Parts[0].(string)
		util.YmapSet(n, "pattern-regex", util.Ystr(re[1:len(re)-1]))
	case parser.NodeName_Fun:
		if len(bt.Parts) == 0 {
			return fmt.Errorf("function with no name: %+v", bt)
		}
		fun, ok := b.functions.Resolve(bt.Parts[0].(string))
		if !ok {
			return fmt.Errorf("undefined function: %+v", bt)
		}

		// validate arguments against parameters
		callargs := make([]string, 0, len(fun.Params))
		if len(bt.Parts) > 1 {
			for _, mv := range bt.Parts[1].(*parser.Named).Parts {
				callargs = append(callargs, mv.(*parser.Named).Parts[0].(string))
			}
		}

		if len(fun.Params) != len(callargs) {
			return fmt.Errorf("%s expects %d args, got %d: %+v", fun.Name, len(fun.Params), len(callargs), bt)
		}

		// push new subsitutuions to replace params with args
		for i, param := range fun.Params {
			b.subs = append(b.subs, &substitution{regexp.MustCompile(`\` + param + `\b`), callargs[i]})
		}

		// build semgrep from the function body and merge it into n
		nn, err := b.buildFrom("brule func", fun.Body)
		if err != nil {
			return err
		}
		n.Content = append(n.Content, nn.Content...)

		// pop subs
		b.subs = b.subs[:len(b.subs)-len(fun.Params)]

	case parser.NodeName_Inside:
		panic("not implemented")
	}
	return nil
}

const (
	ROOT_PARENT = " root "
	DOC_PARENT  = " doc "
	SEQ_PARENT  = " seq "
)

func (b *semgrepBuilder) buildFrom(parentType string, n *yaml.Node) (*yaml.Node, error) {
	nn := *n
	if n.Kind == yaml.ScalarNode {
		// apply substitutions from the top of the subs stack
		s := n.Value
		for i := len(b.subs) - 1; i >= 0; i-- {
			s = b.subs[i].apply(s)
		}
		nn.Value = s
		return &nn, nil
	}

	if parentType == ROOT_PARENT && len(n.Content) > 1 {
		return nil, fmt.Errorf("root contains more than one document: %+v", n)
	}

	nn.Content = make([]*yaml.Node, 0, len(n.Content))

	switch n.Kind {
	case yaml.MappingNode:
		for i := 0; i < len(n.Content)-1; i += 2 {
			kn, vn := n.Content[i], n.Content[i+1]
			// skip root "define" and "include" keys
			if parentType == DOC_PARENT && (kn.Value == "define" || kn.Value == "include") {
				continue
			}
			if kn.Value == "pattern-brule" {
				bt, err := b.parseBrule(vn)
				if err != nil {
					return nil, err
				}
				err = b.appendBrule(&nn, bt)
				if err != nil {
					return nil, err
				}
			} else {
				cvn, err := b.buildFrom(kn.Value, vn)
				if err != nil {
					return nil, err
				}
				nn.Content = append(nn.Content, kn, cvn)
			}
		}
	case yaml.DocumentNode:
		cc, err := b.buildFrom(DOC_PARENT, n.Content[0])
		if err != nil {
			return nil, err
		}
		nn.Content = []*yaml.Node{cc}
	case yaml.ScalarNode:
		panic("handled above")
	case yaml.SequenceNode:
		for _, c := range n.Content {
			cc, err := b.buildFrom(SEQ_PARENT, c)
			if err != nil {
				return nil, err
			}
			nn.Content = append(nn.Content, cc)
		}
	default:
		return nil, fmt.Errorf("unhandled yaml node type: %+v", n)
	}

	return &nn, nil
}
