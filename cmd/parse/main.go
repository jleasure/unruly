package main

import (
	"fmt"
	"os"

	"gitlab.com/gitlab-org/secure/tools/unruly/parser"
)

func main() {
	var exprs []string
	if len(os.Args) > 1 {
		exprs = os.Args[1:]
	} else {
		exprs = []string{
			`f(f($x), $y) 
			where $x matches p, $y matches /cats/`,
			//`f(f($x), $y) where $x matches p, $y matches /cats/`,
			// `(x where $B matches z) where $A matches y`,
			// `a or b and c where $d matches x or y`,
			// `a or b and c where $d matches /.../`,
			// `a or b and c where $d matches e`,
			// `a or b and c where $d has type T`,
			// `in_main and not call_something($A,$B)`,
			// `(a($Y) or a($X1, $X2)) where $X1 matches /a regex!/`,
			// `(a($Y) or a($X1, $X2)) where $X1 matches /a regex!/ or p, $X2 has type T`,
		}
	}
	for _, s := range exprs {
		t, err := parser.Parse(s)
		if err != nil {
			fmt.Printf("%q -> %v\n", s, err)
		} else {
			fmt.Printf("%q -> %v\n", s, t)
		}
	}
}
