package main

import (
	"context"
	"fmt"
	"math"
	"os"
	"path/filepath"

	"github.com/fatih/color"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v3"
	"gitlab.com/gitlab-org/secure/tools/unruly/brule"
	"gitlab.com/gitlab-org/secure/tools/unruly/semgrep"
	"gitlab.com/gitlab-org/secure/tools/unruly/util"
	"gopkg.in/yaml.v3"
)

func parseCommonFlags(cmd *cli.Command) {
	rcmd := cmd.Root()
	if rcmd.Bool("verbose") {
		log.SetLevel(log.DebugLevel)
	}
	col := rcmd.String("color")
	if col == "never" {
		color.NoColor = true
	} else if col == "always" {
		color.NoColor = false
	}
}

func makeCommand(name, description string, transform func(*yaml.Node) (*yaml.Node, error)) *cli.Command {
	return &cli.Command{
		EnableShellCompletion: true,
		Name:                  name,
		Description:           description,
		Arguments: []cli.Argument{&cli.StringArg{
			Name:      "input",
			UsageText: "input file(s)",
			Min:       1,
			Max:       math.MaxInt, // API is broken, should be able to use -1
		}},
		Action: func(ctx context.Context, cmd *cli.Command) error {
			parseCommonFlags(cmd)

			paths := *cmd.Arguments[0].(*cli.StringArg).Values

			for pathNum, path := range paths {
				log.Debugf("Loading %s", path)
				data, err := os.ReadFile(path)
				if err != nil {
					return err
				}
				leadComment, data := util.SplitLeadComment(data)

				var n yaml.Node
				if err := yaml.Unmarshal(data, &n); err != nil {
					return err
				}

				nn, err := transform(&n)

				out, err := yaml.Marshal(nn)
				if err != nil {
					return err
				}

				if pathNum > 0 {
					fmt.Println("---")
				}
				fmt.Print(leadComment)
				fmt.Print(string(out))
			}

			return nil
		},
	}
}

func main() {
	cmd := &cli.Command{
		EnableShellCompletion: true,
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:     "verbose",
				Aliases:  []string{"v"},
				Required: false,
				Value:    false,
			},
			&cli.StringFlag{
				Name:        "color",
				Usage:       "input semgrep file",
				Required:    false,
				DefaultText: "auto|never|always",
				Value:       "auto",
			},
		},
		Commands: []*cli.Command{
			{
				EnableShellCompletion: true,

				Name:        "brule2semgrep",
				Description: "convert brule rules to semgrep",
				Arguments: []cli.Argument{&cli.StringArg{
					Name:      "input",
					UsageText: "input file(s)",
					Min:       1,
					Max:       math.MaxInt, // API is broken, should be able to use -1
				}},
				Action: func(ctx context.Context, cmd *cli.Command) error {
					var err error

					parseCommonFlags(cmd)

					paths := *cmd.Arguments[0].(*cli.StringArg).Values
					for i := 0; i < len(paths); i++ {
						paths[i], err = filepath.Abs(paths[i])
						if err != nil {
							return err
						}
					}

					bruleFiles := make(map[string]*brule.BruleFile)

					// load all brule files
					for _, path := range paths {
						log.Debugf("Loading %s", path)
						bf, ok := bruleFiles[path]
						if ok {
							continue
						}

						bf, err = brule.NewBruleFile(path)
						if err != nil {
							return err
						}
						bruleFiles[path] = bf

						// load includes
						for _, incFile := range bf.Includes {
							bf, ok := bruleFiles[incFile]
							if ok {
								continue
							}
							bf, err = brule.NewBruleFile(incFile)
							if err != nil {
								return err
							}
							bruleFiles[incFile] = bf
						}
					}

					for pathNum, path := range paths {
						bf, ok := bruleFiles[path]
						if !ok {
							return fmt.Errorf("path %q not found in loaded files: %+v", path, bruleFiles)
						}
						stkResolver := brule.NewLayeredFunctionResolver()

						stkResolver.AddResolver(bf.Functions)
						for _, incFile := range bf.Includes {
							stkResolver.AddResolver(bruleFiles[incFile].Functions)
						}

						semgrepYaml, err := bf.ToSemgrep(stkResolver)
						if err != nil {
							return err
						}
						out, err := yaml.Marshal(semgrepYaml)
						if err != nil {
							return err
						}

						// XXX check multi-file output option and handle accordingly
						if pathNum > 0 {
							fmt.Println("---")
						}
						fmt.Print(bf.LeadComment)
						fmt.Print(string(out))
					}
					return nil
				},
			}, {
				EnableShellCompletion: true,
				Name:                  "findmvreuse",
				Description:           "find metavariable reuse in a semgrep file",
				Arguments: []cli.Argument{&cli.StringArg{
					Name:      "input",
					UsageText: "input file(s)",
					Min:       1,
					Max:       math.MaxInt, // API is broken, should be able to use -1
				}},
				Action: func(ctx context.Context, cmd *cli.Command) error {
					parseCommonFlags(cmd)

					paths := *cmd.Arguments[0].(*cli.StringArg).Values

					for _, path := range paths {
						log.Debugf("Loading %s", path)
						data, err := os.ReadFile(path)
						if err != nil {
							return err
						}

						var n yaml.Node
						if err := yaml.Unmarshal(data, &n); err != nil {
							return err
						}
						semgrep.DumpMetavariableReuse(path, &n)
					}

					return nil
				},
			},
			makeCommand("nnf", "apply negation normal form", semgrep.Nnf),
			makeCommand("flatten", "flatten logical operators", semgrep.Flatten),
		},
	}
	if err := cmd.Run(context.Background(), os.Args); err != nil {
		log.Fatal(err)
	}
}
