module gitlab.com/gitlab-org/secure/tools/unruly

go 1.22.2

require (
	github.com/alecthomas/assert/v2 v2.8.1
	github.com/fatih/color v1.16.0
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.9.0
	github.com/urfave/cli/v3 v3.0.0-alpha9
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/alecthomas/repr v0.4.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/hexops/gotextdiff v1.0.3 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/lithammer/dedent v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/openconfig/goyang v1.4.5 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rogpeppe/go-internal v1.11.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sys v0.19.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
