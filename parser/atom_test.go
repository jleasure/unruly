package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAtom(t *testing.T) {
	for _, c := range []struct {
		expr     string
		expected any
	}{
		{`some_var`, nn(NodeName_Fun, "some_var")},
		{`f($a)`, nn(NodeName_Fun, "f", nn(NodeName_Args, nn(NodeName_Meta, "$a")))},
		{`f($a, $b, $c)`, nn(NodeName_Fun, "f", nn(NodeName_Args, nn(NodeName_Meta, "$a"), nn(NodeName_Meta, "$b"), nn(NodeName_Meta, "$c")))},
		{`/some pat (with a group)* and [class] and stuff/`, nn(NodeName_Regex, `/some pat (with a group)* and [class] and stuff/`)},
	} {
		v, err := Parse(c.expr)
		assert.NoErrorf(t, err, "parse error for `%s`", c.expr)
		assert.Equal(t, c.expected, v, testDump(v))
	}
}
