package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCombinations(t *testing.T) {
	for _, c := range []struct {
		expr     string
		expected any
	}{
		{`/a(patt)ern*/ and not f($a) or /[ab]+/`,
			nn(NodeName_Or,
				nn(NodeName_And,
					nn(NodeName_Regex, "/a(patt)ern*/"),
					nn(NodeName_Not,
						nn(NodeName_Fun, "f", nn(NodeName_Args, nn(NodeName_Meta, "$a"))))),
				nn(NodeName_Regex, "/[ab]+/"))},
	} {
		v, err := Parse(c.expr)
		assert.NoErrorf(t, err, "parse error for `%s`", c.expr)
		assert.Equal(t, c.expected, v, testDump(v))
	}
}
