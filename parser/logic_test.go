package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogic(t *testing.T) {
	for _, c := range []struct {
		expr     string
		expected any
	}{
		{`not a`, nn(NodeName_Not, nn(NodeName_Fun, "a"))},
		{`a and not b`, nn(NodeName_And, nn(NodeName_Fun, "a"), nn(NodeName_Not, nn(NodeName_Fun, "b")))},
		{`a or b`, nn(NodeName_Or, nn(NodeName_Fun, "a"), nn(NodeName_Fun, "b"))},
		{`a or b or c`, nn(NodeName_Or, nn(NodeName_Fun, "a"), nn(NodeName_Fun, "b"), nn(NodeName_Fun, "c"))},
		{`a   or   b  or c  `, nn(NodeName_Or, nn(NodeName_Fun, "a"), nn(NodeName_Fun, "b"), nn(NodeName_Fun, "c"))},
		{`a or b and c`, nn(NodeName_Or, nn(NodeName_Fun, "a"), nn(NodeName_And, nn(NodeName_Fun, "b"), nn(NodeName_Fun, "c")))},
		{`a and b or c`, nn(NodeName_Or, nn(NodeName_And, nn(NodeName_Fun, "a"), nn(NodeName_Fun, "b")), nn(NodeName_Fun, "c"))},
		{`(a or b) and c`, nn(NodeName_And, nn(NodeName_Or, nn(NodeName_Fun, "a"), nn(NodeName_Fun, "b")), nn(NodeName_Fun, "c"))},
	} {
		v, err := Parse(c.expr)
		assert.NoErrorf(t, err, "parse error for `%s`", c.expr)
		assert.Equal(t, c.expected, v, testDump(v))
	}
}
