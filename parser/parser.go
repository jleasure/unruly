/*
- add all metavariable-* variants to where_clause
- lint: metavariables are scoped to conjunctions, so if a metavariable name is
reused across a disjunction, suggest a rename.

see
https://semgrep.dev/docs/writing-rules/rule-syntax/#metavariables-in-logical-ors
*/
package parser

import (
	"fmt"
	"regexp"
	"strings"
	"unicode"
)

type Rule func(s string) (int, any, error)

var NoMatch = fmt.Errorf("no match")

type EOT_type struct{}

func (EOT_type) String() string {
	return "<eot>"
}

var EOT EOT_type = EOT_type{}

func eot(s string) (int, any, error) {
	if len(s) == 0 {
		return 0, EOT, nil
	}
	return 0, nil, NoMatch
}

func tok(v string) Rule {
	return func(s string) (int, any, error) {
		if strings.HasPrefix(s, v) {
			// eat spaces
			pos := len(v)
			ss := s[pos:]
			pos += len(ss) - len(strings.TrimLeftFunc(ss, unicode.IsSpace))
			return pos, v, nil
		}
		return 0, nil, NoMatch
	}
}

func re(pat string) Rule {
	re := regexp.MustCompile("^(" + pat + ")\\s*")
	re.Longest()

	return func(s string) (int, any, error) {
		loc := re.FindStringSubmatchIndex(s)
		if loc == nil || loc[1] == 0 {
			return 0, nil, NoMatch
		}
		return loc[1], s[loc[2]:loc[3]], nil
	}
}

func alt(rules ...Rule) Rule {
	return func(s string) (int, any, error) {
		for _, r := range rules {
			consumed, v, err := r(s)
			if err == nil {
				return consumed, v, nil
			} else if err == NoMatch {
				continue
			} else {
				return 0, nil, err
			}
		}
		return 0, nil, NoMatch
	}
}

func cat(rules ...Rule) Rule {
	return func(s string) (int, any, error) {
		pos := 0
		t := make([]any, 0, len(rules))
		for _, r := range rules {
			consumed, v, err := r(s[pos:])
			if err == nil {
				pos += consumed
				t = append(t, v)
			} else if err == NoMatch {
				return 0, nil, NoMatch
			} else {
				return 0, nil, err
			}
		}
		return pos, t, nil
	}
}

func flatten(pl *[]any, x any, filter func(any) bool) {
	if l, ok := x.([]any); ok {
		for _, v := range l {
			flatten(pl, v, filter)
		}
	} else {
		if filter(x) {
			*pl = append(*pl, x)
		}
	}
}

type FlattenOption int

const (
	SkipStrs FlattenOption = iota
)

func is_string(v any) bool {
	_, ok := v.(string)
	return ok
}

// flattenting cat
func fcat(rules_and_options ...any) Rule {
	rules := make([]Rule, 0, len(rules_and_options))
	skipping_strs := false

	for _, arg := range rules_and_options {
		switch a := arg.(type) {
		case func(s string) (int, any, error):
			rules = append(rules, a)
		case Rule:
			rules = append(rules, a)
		case FlattenOption:
			switch a {
			case SkipStrs:
				skipping_strs = true
			}
		default:
			panic("invalid argument to fcat, arg must be a Rule or a FlattenOption")
		}
	}

	return func(s string) (int, any, error) {
		pos := 0
		t := make([]any, 0, len(rules))
		for _, r := range rules {
			consumed, v, err := r(s[pos:])
			if err == nil {
				pos += consumed

				flatten(&t, v, func(v any) bool {
					return !(skipping_strs && is_string(v))
				})
			} else if err == NoMatch {
				return 0, nil, NoMatch
			} else {
				return 0, nil, err
			}
		}
		return pos, t, nil
	}
}

func rep(r Rule) Rule {
	return func(s string) (int, any, error) {
		pos := 0
		t := make([]any, 0)
		for {
			consumed, v, err := r(s[pos:])
			if err == nil {
				pos += consumed
				t = append(t, v)
			} else if err == NoMatch {
				break
			} else {
				return 0, nil, err
			}
		}
		return pos, t, nil
	}
}

func opt(r Rule) Rule {
	return func(s string) (int, any, error) {
		consumed, v, err := r(s)
		if err == NoMatch {
			return 0, []any{}, nil
		}
		return consumed, v, err
	}
}

type NodeName string

func (name NodeName) String() string {
	return string(name)
}

func (name NodeName) constName() string {
	return "NodeName_" + string(name)
}

const (
	NodeName_Inside NodeName = "Inside"
	NodeName_Regex  NodeName = "Regex"
	NodeName_Meta   NodeName = "Meta"
	NodeName_Not    NodeName = "Not"
	NodeName_Or     NodeName = "Or"
	NodeName_Args   NodeName = "Args"
	NodeName_And    NodeName = "And"
	NodeName_Where  NodeName = "Where"
	NodeName_Type   NodeName = "Type"
	NodeName_Fun    NodeName = "Fun"
)

type Named struct {
	Name  NodeName
	Parts []any
}

func (n Named) String() string {
	var sb strings.Builder
	sb.WriteString(string(n.Name))
	sb.WriteRune('(')
	if len(n.Parts) > 0 {
		sb.WriteString(fmt.Sprintf("%v", n.Parts[0]))
	}
	for _, v := range n.Parts[1:] {
		sb.WriteString(", ")
		sb.WriteString(fmt.Sprintf("%v", v))
	}
	sb.WriteRune(')')
	return sb.String()
}

type NamedOption int

const (
	Collapse NamedOption = iota
)

func n(name NodeName, r Rule, opts ...NamedOption) Rule {
	collapsing := false

	for _, opt := range opts {
		switch opt {
		case Collapse:
			collapsing = true
		}
	}

	return func(s string) (int, any, error) {
		consumed, v, err := r(s)
		if err != nil {
			return 0, nil, err
		}
		parts, ok := v.([]any)
		if !ok {
			parts = []any{v}
		}
		if collapsing && len(parts) == 1 {
			return consumed, parts[0], nil
		}

		return consumed, &Named{name, parts}, nil
	}
}

type fref struct {
	f Rule
}

func (r *fref) r(s string) (int, any, error) {
	return r.f(s)
}

func Parser() Rule {
	//// brule grammar

	// forward references
	r_expr_ := &fref{}
	r_fun_ := &fref{}

	//
	// rules are listed in descending precedence
	//
	// atomics
	r_id := re("[a-zA-Z_][a-zA-Z0-9_]*")
	r_meta := n(NodeName_Meta, re("\\$[a-zA-Z_][a-zA-Z0-9_]*"))
	r_arg := alt(r_meta, r_fun_.r)
	r_args := n(NodeName_Args, fcat(tok("("), r_arg, rep(cat(tok(","), r_arg)), tok(")"), SkipStrs))
	r_fun := n(NodeName_Fun, fcat(r_id, opt(r_args)))
	r_regex := n(NodeName_Regex, re(`\/((?:[^\r\n\[/\\]|\\.|\[(?:[^\r\n\]\\]|\\.)*\])+)\/`))
	r_atom := alt(r_regex, r_fun, cat(tok("("), r_expr_.r, tok(")")))

	// logic
	r_not := n(NodeName_Not, fcat(tok("not"), r_atom, SkipStrs))
	r_pred := alt(r_not, r_atom)

	r_and := n(NodeName_And, fcat(r_pred, rep(cat(tok("and"), r_pred)), SkipStrs), Collapse)
	r_where_clause := cat(r_meta, alt(
		fcat(tok("matches"), r_and, SkipStrs),
		fcat(tok("has"), tok("type"), n(NodeName_Type, r_id), SkipStrs),
	))
	r_where := n(NodeName_Where, fcat(r_and, tok("where"), r_where_clause, rep(cat(tok(","), r_where_clause)), SkipStrs))
	r_where_and := alt(r_where, r_and)

	r_or := n(NodeName_Or, fcat(r_where_and, rep(cat(tok("or"), r_where_and)), SkipStrs), Collapse)

	r_expr := r_or

	// store forward references
	r_expr_.f = r_expr
	r_fun_.f = r_fun

	// top
	r_brule := cat(r_expr, eot)
	return r_brule
}

var r_brule Rule = Parser()

func Parse(s string) (*Named, error) {
	_, v, err := r_brule(s)
	if err != nil {
		return nil, err
	}
	if l, ok := v.([]any); ok && len(l) == 2 {
		n := l[0]
		if nn, ok := n.(*Named); ok {
			return nn, nil
		}
		return nil, fmt.Errorf("expected top node of parse to be named: %+v", n)
	}
	panic("r_brule should parse to a slice with two elements")
}

// test api follows
func nn(name NodeName, parts ...any) *Named {
	return &Named{name, parts}
}

func testDump(v any) string {
	if named, ok := v.(*Named); ok {
		var sb strings.Builder
		fmt.Fprintf(&sb, "nn(%s", named.Name.constName())
		for _, part := range named.Parts {
			sb.WriteString(", ")
			sb.WriteString(testDump(part))
		}
		sb.WriteRune(')')
		return sb.String()
	}
	return fmt.Sprintf("%q", v)
}

func TryNamed(bt any, name NodeName) ([]any, error) {
	if bt == nil {
		return nil, fmt.Errorf("tryNamed called with nil parsetree")
	}

	if n, ok := bt.(*Named); ok {
		if n.Name != name {
			return nil, fmt.Errorf("named node is not of type %s: %+v", name, bt)
		}
		return n.Parts, nil
	}
	return nil, fmt.Errorf("node is not named: %+v", bt)
}
