package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWhere(t *testing.T) {
	for _, c := range []struct {
		expr     string
		expected any
	}{
		{`a($X) where $X matches /some pat/`,
			nn(NodeName_Where,
				nn(NodeName_Fun,
					"a",
					nn(NodeName_Args, nn(NodeName_Meta, "$X")),
				),

				nn(NodeName_Meta, "$X"),
				nn(NodeName_Regex, "/some pat/"))},
		{`a where $b matches c, $d matches e`,
			nn(NodeName_Where,
				nn(NodeName_Fun, "a"),

				nn(NodeName_Meta, "$b"),
				nn(NodeName_Fun, "c"),

				nn(NodeName_Meta, "$d"),
				nn(NodeName_Fun, "e"))},
	} {
		v, err := Parse(c.expr)
		assert.NoErrorf(t, err, "parse error for `%s`", c.expr)
		assert.Equal(t, c.expected, v, testDump(v))
	}
}
