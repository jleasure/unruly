package semgrep

import (
	"fmt"

	"gitlab.com/gitlab-org/secure/tools/unruly/util"
	"gopkg.in/yaml.v3"
)

// FixMetavariablePatterns converts a logical but disallowed metavariable-pattern into a valid one.
func FixMetavariablePatterns(n *yaml.Node) (*yaml.Node, error) {
	v := fixmvpatternsvisitor{}
	return v.visit(n)
}

type fixmvpatternsvisitor struct {
}

func (v *fixmvpatternsvisitor) visitKV(k string, vn *yaml.Node) (string, *yaml.Node, error) {
	vn, err := v.visit(vn)
	if err != nil {
		return k, nil, err
	}
	switch k {
	case "metavariable-pattern":
		if vn.Kind != yaml.MappingNode {
			return k, nil, fmt.Errorf("invalid value node of %s: %v", k, vn)
		}
		if len(vn.Content) != 4 {
			return k, nil, fmt.Errorf("invalid contents for value node of %s: %v", k, vn.Content)
		}
		var i int
		if vn.Content[0].Value == "metavariable" {
			i = 2
		} else {
			if vn.Content[2].Value == "metavariable" {
				i = 0
			} else {
				return k, nil, fmt.Errorf("invalid contents for value node of %s: %v", k, vn.Content)
			}
		}

		switch vn.Content[i].Value {
		case "pattern-not-regex":
			fallthrough
		case "pattern-not":
			// pattern-not: ...   ->  patterns: [pattern-not:...]
			vn.Content[i+1] = util.Yseq(util.Ymap(vn.Content[i], vn.Content[i+1]))
			vn.Content[i] = util.Ystr("patterns")
			return k, vn, nil
		}
	}
	return k, vn, err
}

func (v *fixmvpatternsvisitor) visit(n *yaml.Node) (*yaml.Node, error) {
	nn := *n
	switch n.Kind {
	case yaml.ScalarNode:
		// nothing else to do
	case yaml.MappingNode:
		nn.Content = make([]*yaml.Node, 0, len(n.Content))
		for i := 0; i < len(n.Content)-1; i += 2 {
			kn, vn := n.Content[i], n.Content[i+1]
			k, vn, err := v.visitKV(kn.Value, vn)
			if err != nil {
				return nil, err
			}
			nn.Content = append(nn.Content, util.Ystr(k), vn)
		}
	default:
		nn.Content = make([]*yaml.Node, 0, len(n.Content))
		for _, c := range n.Content {
			c, err := v.visit(c)
			if err != nil {
				return nil, err
			}
			nn.Content = append(nn.Content, c)
		}
	}
	return &nn, nil
}
