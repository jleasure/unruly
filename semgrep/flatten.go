package semgrep

import (
	"fmt"

	"gitlab.com/gitlab-org/secure/tools/unruly/util"
	"gopkg.in/yaml.v3"
)

// Flatten flattens nested occurences of patterns and pattern-either in semgrep YAML
func Flatten(n *yaml.Node) (*yaml.Node, error) {
	v := flattenvisitor{}
	return v.visit(n)
}

type flattenvisitor struct {
}

func (v *flattenvisitor) visitKV(k string, vn *yaml.Node) (string, *yaml.Node, error) {
	vn, err := v.visit(vn)
	if err != nil {
		return k, nil, err
	}
	switch k {
	case "pattern-sources":
		fallthrough
	case "pattern-sanitizers":
		fallthrough
	case "pattern-sinks":
		if vn.Kind != yaml.SequenceNode {
			return k, nil, fmt.Errorf("invalid value node of %s: %q", k, util.Y2string(vn))
		}

		content := make([]*yaml.Node, 0, len(vn.Content))
		for _, e := range vn.Content {
			if e.Kind != yaml.MappingNode {
				return k, nil, fmt.Errorf("invalid entry in value node of %s: %q", k, util.Y2string(e))
			}
			if len(e.Content) == 2 {
				// flatten pattern-either children
				if e.Content[0].Value == "pattern-either" {
					content = append(content, e.Content[1].Content...)
				} else {
					content = append(content, e)
				}
			} else {
				// do not inspect maps with unknown structure
				content = append(content, e)
			}
		}
		vn.Content = content
	case "patterns":
		fallthrough
	case "pattern-either":
		if vn.Kind != yaml.SequenceNode {
			return k, nil, fmt.Errorf("invalid value node of %s: %q", k, util.Y2string(vn))
		}

		content := make([]*yaml.Node, 0, len(vn.Content))
		for _, e := range vn.Content {
			if e.Kind != yaml.MappingNode {
				return k, nil, fmt.Errorf("invalid entry in value node of %s: %q", k, util.Y2string(e))
			}
			if len(e.Content) == 2 {
				// collapse
				if len(vn.Content) == 1 {
					return e.Content[0].Value, e.Content[1], nil
				}

				// flatten
				if e.Content[0].Value == k {
					content = append(content, e.Content[1].Content...)
				} else {
					content = append(content, e)
				}
			} else {
				// do not inspect maps with unknown structure
				content = append(content, e)
			}
		}
		vn.Content = content
	}
	return k, vn, err
}

func (v *flattenvisitor) visit(n *yaml.Node) (*yaml.Node, error) {
	nn := *n
	switch n.Kind {
	case yaml.ScalarNode:
		// nothing else to do
	case yaml.MappingNode:
		nn.Content = make([]*yaml.Node, 0, len(n.Content))
		for i := 0; i < len(n.Content)-1; i += 2 {
			kn, vn := n.Content[i], n.Content[i+1]
			k, vn, err := v.visitKV(kn.Value, vn)
			if err != nil {
				return nil, err
			}
			nn.Content = append(nn.Content, util.Ystr(k), vn)
		}
	default:
		nn.Content = make([]*yaml.Node, 0, len(n.Content))
		for _, c := range n.Content {
			c, err := v.visit(c)
			if err != nil {
				return nil, err
			}
			nn.Content = append(nn.Content, c)
		}
	}
	return &nn, nil
}
