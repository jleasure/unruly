package semgrep

import (
	"testing"

	"github.com/alecthomas/assert/v2"
	"github.com/lithammer/dedent"
	"gopkg.in/yaml.v3"
)

func TestFlatten(t *testing.T) {
	for _, c := range []*struct {
		y string
		e string
	}{
		// source valued key negations
		{`
		patterns:
		  - patterns:
		    - pattern: stuff
		`, `
		pattern: stuff
		`},
		{`
		pattern-either:
		  - patterns:
		    - pattern: stuff
		`, `
		pattern: stuff
		`},
		{`
		patterns:
		  - patterns:
		    - pattern: stuff1
		    - pattern: stuff2
		  - pattern: stuff3
		  - pattern: stuff4
		`, `
		patterns:
		  - pattern: stuff1
		  - pattern: stuff2
		  - pattern: stuff3
		  - pattern: stuff4
		`},
		{`
		patterns:
		  - patterns:
		    - pattern: stuff1
		    - pattern: stuff2
		  - patterns:
		    - pattern: stuff3
		    - pattern: stuff4
		`, `
		patterns:
		  - pattern: stuff1
		  - pattern: stuff2
		  - pattern: stuff3
		  - pattern: stuff4
		`},
		{`
		pattern-either:
		  - pattern-either:
		    - pattern: stuff1
		    - pattern: stuff2
		  - pattern-either:
		    - pattern: stuff3
		    - pattern: stuff4
		`, `
		pattern-either:
		  - pattern: stuff1
		  - pattern: stuff2
		  - pattern: stuff3
		  - pattern: stuff4
		`},
	} {
		var y, e yaml.Node

		err := yaml.Unmarshal([]byte(dedent.Dedent(c.y[1:])), &y)
		assert.NoError(t, err)

		err = yaml.Unmarshal([]byte(dedent.Dedent(c.e[1:])), &e)
		assert.NoError(t, err)

		a, err := Flatten(&y)
		assert.NoError(t, err)

		assertEqualYaml(t, &e, a)
	}

}
