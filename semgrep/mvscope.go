package semgrep

import (
	"fmt"
	"regexp"
	"sort"

	"github.com/fatih/color"
	"github.com/openconfig/goyang/pkg/indent"
	"gitlab.com/gitlab-org/secure/tools/unruly/util"
	"gopkg.in/yaml.v3"
)

var metaPat = regexp.MustCompile("\\$[a-zA-Z_][a-zA-Z0-9_]*")

func init() {
	metaPat.Longest()
}

type mvOccurence struct {
	n   *yaml.Node
	loc []int
}

// DumpMetavariableReuse identifies metavariables with independent bindings.
//
//	see https://semgrep.dev/docs/writing-rules/rule-syntax/#metavariables-in-logical-ors
func DumpMetavariableReuse(path string, n *yaml.Node) {
	v := scopevisitor{util.NewUnionFind[yaml.Node](), make(map[*yaml.Node][]*yaml.Node)}
	v.visit(n)
	v.cs.Compress()
	parts := v.cs.Parts()

	// split by variables
	byvar := make(map[string]map[*yaml.Node][]*mvOccurence)
	for partId, part := range parts {
		for _, sc := range part {
			// for each variable in this part, append the occurrence to byvar[var][part]
			for _, mvLoc := range metaPat.FindAllStringIndex(sc.Value, -1) {
				o := mvOccurence{sc, mvLoc}
				mvName := sc.Value[mvLoc[0]:mvLoc[1]]
				mvParts, ok := byvar[mvName]
				if !ok {
					mvParts = make(map[*yaml.Node][]*mvOccurence)
					byvar[mvName] = mvParts
				}
				l := mvParts[partId]
				l = append(l, &o)
				mvParts[partId] = l
			}
		}
	}
	red := color.RedString
	for mvName, mvParts := range byvar {
		if len(mvParts) > 1 {
			parts := make([][]*mvOccurence, 0, len(mvParts))
			for _, part := range mvParts {
				parts = append(parts, part)
				sort.Slice(part, func(i, j int) bool {
					return part[i].n.Line < part[j].n.Line
				})
			}
			sort.Slice(parts, func(i, j int) bool {
				return parts[i][0].n.Line < parts[j][0].n.Line
			})

			fmt.Printf("%s: %s is reused in independent contexts\n", path, red(mvName))
			for _, part := range parts {

				fmt.Println("  ===")
				for _, o := range part {
					fmt.Printf("  %s:%d:%d\n", path, o.n.Line, o.n.Column)
					b, e := o.loc[0], o.loc[1]
					s := o.n.Value[b:e]
					s = o.n.Value[:b] + red("%s", s) + o.n.Value[e:]
					s = indent.String("    ", s)
					fmt.Println(s)
				}
			}
			fmt.Println()
		}
	}
}

type scopevisitor struct {
	// coincident scalars, e.g. metavariables in these patterns may be bound together
	cs *util.UnionFind[yaml.Node]

	// scalars cache
	sc map[*yaml.Node][]*yaml.Node
}

// unite scalars across all conjoined pairs
func (v *scopevisitor) scalars(n *yaml.Node, pl *[]*yaml.Node) {
	if scl, ok := v.sc[n]; ok {
		*pl = append(*pl, scl...)
		return
	}

	switch n.Kind {
	case yaml.ScalarNode:
		*pl = append(*pl, n)
	case yaml.MappingNode:
		for i := 0; i < len(n.Content)-1; i += 2 {
			v.scalars(n.Content[i+1], pl)
		}
	default:
		for _, c := range n.Content {
			v.scalars(c, pl)
		}
	}
}

func (v *scopevisitor) visitKV(k string, vn *yaml.Node) {
	v.visit(vn)

	switch k {
	case "patterns":
		cscl := make([][]*yaml.Node, len(vn.Content))
		for i, c := range vn.Content {
			v.scalars(c, &cscl[i])
			v.sc[c] = cscl[i]
		}

		// union scalar nodes across terms of conjuction
		for i, iscl := range cscl {
			for _, jscl := range cscl[i+1:] {
				for _, isc := range iscl {
					for _, jsc := range jscl {
						v.cs.Union(isc, jsc)
					}
				}
			}
		}

	default:
	}
}

func (v *scopevisitor) visit(n *yaml.Node) {
	switch n.Kind {
	case yaml.ScalarNode:
		v.cs.Find(n)
	case yaml.MappingNode:
		for i := 0; i < len(n.Content)-1; i += 2 {
			kn, vn := n.Content[i], n.Content[i+1]
			v.visitKV(kn.Value, vn)
		}
	default:
		for _, c := range n.Content {
			v.visit(c)
		}
	}
}
