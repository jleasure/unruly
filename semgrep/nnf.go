package semgrep

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/secure/tools/unruly/util"
	"gopkg.in/yaml.v3"
)

// fix "naive" semgrep by pushing negations down
func Nnf(n *yaml.Node) (*yaml.Node, error) {
	v := nnfvisitor{}
	return v.visit(n, false)
}

type nnfvisitor struct {
}

func negateKey(k string) string {
	if strings.Contains(k, "-not") {
		return strings.Replace(k, "-not", "", 1)
	}
	i := strings.Index(k, "-")
	if i == -1 {
		i = len(k)
	}
	return k[:i] + "-not" + k[i:]
}

func (v *nnfvisitor) visitKV(k string, vn *yaml.Node, negated bool) (string, *yaml.Node, error) {
	switch k {
	case "pattern-not":
		negated = !negated
		switch vn.Kind {
		case yaml.ScalarNode:
			if !negated {
				k = "pattern"
			}
			return k, util.Ystr(vn.Value), nil
		case yaml.SequenceNode:
			return k, nil, fmt.Errorf("pattern-not cannot have an array value")
		case yaml.MappingNode:
			if len(vn.Content) == 2 {
				k, vn = vn.Content[0].Value, vn.Content[1]
				return v.visitKV(k, vn, negated)
			}
			return k, nil, fmt.Errorf("unhandled: pattern-not with map value having more than one entry: %q", util.Y2string(vn))
		default:
			panic("unexpected child")
		}
	case "pattern-not-regex":
		fallthrough
	case "pattern-not-inside":
		fallthrough
	case "pattern":
		fallthrough
	case "pattern-regex":
		fallthrough
	case "pattern-inside":
		if negated {
			k = negateKey(k)
		}
	case "pattern-either":
		if negated {
			k = "patterns"
		}
	case "patterns":
		if negated {
			k = "pattern-either"
		}

	case "metavariable-pattern":
		fallthrough
	case "metavariable-regex":
		fallthrough
	case "metavariable-type":
		if negated {
			panic("negation of metavariables not handled")
		}
	default:
		// assume that negation is irrelevant
	}
	if vn.Kind == yaml.ScalarNode {
		return k, vn, nil
	}

	vn, err := v.visit(vn, negated)
	if err != nil {
		return k, nil, err
	}
	return k, vn, nil
}

func (v *nnfvisitor) visit(n *yaml.Node, negated bool) (*yaml.Node, error) {
	nn := *n
	var err error
	switch n.Kind {
	case yaml.MappingNode:
		nn.Content = make([]*yaml.Node, 0, len(n.Content))
		for i := 0; i < len(n.Content)-1; i += 2 {
			kn, vn := n.Content[i], n.Content[i+1]
			k, vn, err := v.visitKV(kn.Value, vn, negated)
			if err != nil {
				return nil, err
			}
			util.YmapSet(&nn, k, vn)
		}
	case yaml.SequenceNode:
		fallthrough
	case yaml.DocumentNode:
		fallthrough
	default:
		nn.Content, err = v.visitSlice(n.Content, negated)
	}
	return &nn, err
}

func (v *nnfvisitor) visitSlice(l []*yaml.Node, negated bool) ([]*yaml.Node, error) {
	ll := make([]*yaml.Node, 0, len(l))
	for _, n := range l {
		nn, err := v.visit(n, negated)
		if err != nil {
			return nil, err
		}
		ll = append(ll, nn)
	}
	return ll, nil
}
