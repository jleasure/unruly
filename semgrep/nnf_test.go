package semgrep

import (
	"strings"
	"testing"

	"github.com/alecthomas/assert/v2"
	"github.com/lithammer/dedent"
	"gitlab.com/gitlab-org/secure/tools/unruly/util"
	"gopkg.in/yaml.v3"
)

func assertEqualYaml(t testing.TB, e, a *yaml.Node) {
	t.Helper()

	aout, err := yaml.Marshal(a.Content[0])
	assert.NoError(t, err)

	eout, err := yaml.Marshal(e.Content[0])
	assert.NoError(t, err)

	var sb strings.Builder

	if !util.Yeq(e, a) {
		sb.WriteString("\nexpected:\n")
		sb.Write(eout)
		sb.WriteString("\nactual:\n")
		sb.Write(aout)
		t.Fatal("Expected and actual differ", sb.String())
	}
}

func TestNegation(t *testing.T) {
	for _, c := range []*struct {
		y string
		e string
	}{
		// source valued key negations
		{`
		pattern-not:
		  pattern: stuff
		`, `
		pattern-not: stuff
		`},

		{`
		pattern-not:
		  pattern-not: stuff
		`, `
		pattern: stuff
		`},

		{`
		pattern-not:
		  pattern-inside: stuff
		`, `
		pattern-not-inside: stuff
		`},

		{`
		pattern-not:
		  pattern-not-inside: stuff
		`, `
		pattern-inside: stuff
		`},

		{`
		pattern-not:
		  pattern-regex: stuff
		`, `
		pattern-not-regex: stuff
		`},

		{`
		pattern-not:
		  pattern-not-regex: stuff
		`, `
		pattern-regex: stuff
		`},

		// De Morgan's
		{`
		pattern-not:
		  pattern-either:
		    - pattern: stuff1
		    - pattern: stuff2
		`, `
		patterns:
		  - pattern-not: stuff1
		  - pattern-not: stuff2
		`},

		{`
		pattern-not:
		  patterns:
		    - pattern: stuff1
		    - pattern: stuff2
		`, `
		pattern-either:
		  - pattern-not: stuff1
		  - pattern-not: stuff2
		`},

		// combination cases
		{`
		pattern-not:
		  patterns:
		    - pattern-not: stuff1
		    - pattern-either:
		        - pattern: stuff2
		        - pattern-not: stuff3
		`, `
		pattern-either:
		- pattern: stuff1
		- patterns:
		    - pattern-not: stuff2
		    - pattern: stuff3
		`},

		{`
		patterns:
		  - pattern-not:
		      pattern-either:
		        - patterns:
		            - pattern: stuff1
		            - pattern-not: stuff2
		        - pattern-not: stuff3
		  - pattern: stuff4
		`, `
		patterns:
		  - patterns:
		      - pattern-either:
		          - pattern-not: stuff1
		          - pattern: stuff2
		      - pattern: stuff3
		  - pattern: stuff4
		`},
	} {
		var y, e yaml.Node

		err := yaml.Unmarshal([]byte(dedent.Dedent(c.y[1:])), &y)
		assert.NoError(t, err)

		err = yaml.Unmarshal([]byte(dedent.Dedent(c.e[1:])), &e)
		assert.NoError(t, err)

		a, err := Nnf(&y)
		assert.NoError(t, err)

		assertEqualYaml(t, &e, a)

	}

}
