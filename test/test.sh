#!/bin/bash

D=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
UNRULY="$D/../unruly"

total=0
pass=0
for bf in "$D"/data/*-brule.yml; do
    sf=${bf/-brule/-semgrep}

    echo "diff $bf $sf"
    if [ -f "$sf" ]; then
        if "$UNRULY" brule2semgrep "$bf" | diff "$sf" -; then
            echo "  PASS"
            ((pass++))
        else
            echo "  FAIL"
        fi
        ((total++))
    else
        echo "  GENERATING!!!"
        "$UNRULY" brule2semgrep "$bf" > "$sf"
    fi
done

echo "$pass/$total passed"
