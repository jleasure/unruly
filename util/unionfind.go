package util

import "unsafe"

type UnionFind[T any] struct {
	parent map[*T]*T
}

func NewUnionFind[T any]() *UnionFind[T] {
	return &UnionFind[T]{make(map[*T]*T)}
}

func (uf *UnionFind[T]) Find(p *T) *T {
	// no path compression
	for {
		pp, ok := uf.parent[p]
		if !ok {
			uf.parent[p] = p
			return p
		}
		if pp == p {
			return p
		}
		p = pp
	}
}

func ptrless[T any](p1, p2 *T) bool {
	return uintptr(unsafe.Pointer(p1)) < uintptr(unsafe.Pointer(p2))
}

// return their common representative
func (uf *UnionFind[T]) Union(p1, p2 *T) *T {
	r1, r2 := uf.Find(p1), uf.Find(p2)
	if r1 == r2 {
		return r1
	}
	if ptrless(r2, r1) {
		r1, r2 = r2, r1
	}

	uf.parent[r2] = r1
	return r1
}

func (uf *UnionFind[T]) Compress() {
	for p, pp := range uf.parent {
		uf.parent[p] = uf.Find(pp)
	}
}

func (uf *UnionFind[T]) Parts() map[*T][]*T {
	parts := make(map[*T][]*T)
	for p := range uf.parent {
		pp := uf.Find(p)
		l := parts[pp]
		l = append(l, p)
		parts[pp] = l
	}
	return parts
}
