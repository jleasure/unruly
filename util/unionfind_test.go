package util

import (
	"testing"

	"github.com/alecthomas/assert/v2"
)

type st struct {
	s string
}

func TestUnionFind(t *testing.T) {
	uf := NewUnionFind[st]()
	l := []*st{
		{"a"},
		{"b"},
		{"c"},
		{"d"},
		{"e"},
		{"f"},
	}

	for _, p := range l {
		uf.Find(p)
	}

	parts := uf.Parts()
	for _, pl := range parts {
		assert.Equal(t, 1, len(pl))
	}

	uf.Compress()
	parts = uf.Parts()
	for _, pl := range parts {
		assert.Equal(t, 1, len(pl))
	}

	uf.Union(l[0], l[1])
	uf.Union(l[1], l[2])

	parts = uf.Parts()
	assert.Equal(t, 3, len(parts[l[0]]))
	for i := 3; i < 6; i++ {
		assert.Equal(t, 1, len(parts[l[i]]))
	}

	uf.Compress()
	parts = uf.Parts()
	assert.Equal(t, 3, len(parts[l[0]]))
	for i := 3; i < 6; i++ {
		assert.Equal(t, 1, len(parts[l[i]]))
	}

}
