package util

import (
	"fmt"
	"reflect"
	"strings"

	"gopkg.in/yaml.v3"
)

// provides (a limited) ability to unmarshal yaml into a struct with a
// "yaml:,remaining" tag by collecting unparsed fields into a yaml.Node.  This
// is similar to "yaml:,inline", but the type is a yaml.Node instead of a map.
func UnmarshalWithRemaining(node *yaml.Node, out any) error {
	if node.Kind != yaml.MappingNode {
		return fmt.Errorf("expected mapping node, got %v", node.Kind)
	}

	fields := make(map[string]any)

	v := reflect.ValueOf(out).Elem()

	var premaining **yaml.Node

	for i := 0; i < v.NumField(); i++ {
		field := v.Type().Field(i)
		tag, ok := field.Tag.Lookup("yaml")
		if !ok {
			continue
		}
		if tag == ",remaining" {
			premaining, ok = v.Field(i).Addr().Interface().(**yaml.Node)
			if !ok {
				return fmt.Errorf("field marked remaining has wrong type: %T", v.Field(i).Interface())
			}
			continue
		}

		if tag == "-" || strings.Contains(tag, ",") {
			return fmt.Errorf("cannot parse tag: %q", tag)
		}
		fields[tag] = v.Field(i).Addr().Interface()
	}

	remainingContent := make([]*yaml.Node, 0, len(node.Content))
	for i := 0; i < len(node.Content); i += 2 {
		key := node.Content[i]
		if key.Kind != yaml.ScalarNode {
			return fmt.Errorf("map key is not a scalar: %+v", key)
		}
		value := node.Content[i+1]
		if p, ok := fields[key.Value]; ok {
			if err := value.Decode(p); err != nil {
				return err
			}
		} else {
			remainingContent = append(remainingContent, key, value)
		}
	}

	newNode := *node
	newNode.Content = remainingContent
	if premaining != nil {
		*premaining = &newNode
	}
	return nil
}
