package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
)

func TestUnmarshalWithRemaining(t *testing.T) {
	data := []byte(`
name: john
args: [a,b,c]
other:
    - stuff
    - and
    - things
`)

	var s struct {
		Name string   `yaml:"name"`
		Args []string `yaml:"args"`

		Body *yaml.Node `yaml:",remaining"`
	}

	var node yaml.Node
	err := yaml.Unmarshal(data, &node)
	assert.NoError(t, err)
	err = UnmarshalWithRemaining(node.Content[0], &s)
	assert.NoError(t, err)

	assert.Equal(t, "john", s.Name)
	assert.Equal(t, []string{"a", "b", "c"}, s.Args)

	out, err := yaml.Marshal(s.Body)
	assert.NoError(t, err)

	assert.Equal(t, []byte(`other:
    - stuff
    - and
    - things
`), out)
}
