package util

import (
	"regexp"

	"gopkg.in/yaml.v3"
)

func Ystr(s string) *yaml.Node {
	return &yaml.Node{
		Kind: yaml.ScalarNode,
		// Style: yaml.DoubleQuotedStyle,
		Value: s,
		Tag:   "!!str",
	}
}

func Yseq(content ...*yaml.Node) *yaml.Node {
	return &yaml.Node{
		Kind:    yaml.SequenceNode,
		Value:   "",
		Tag:     "!!seq",
		Content: content,
	}
}

func Ymap(content ...*yaml.Node) *yaml.Node {
	return &yaml.Node{
		Kind:    yaml.MappingNode,
		Value:   "",
		Tag:     "!!map",
		Content: content,
	}
}

func YmapSet(n *yaml.Node, key string, vn *yaml.Node) {
	n.Content = append(n.Content, Ystr(key), vn)
}

// compare only values, not comments or style. map keys must be in the same order.
func Yeq(n1 *yaml.Node, n2 *yaml.Node) bool {
	if n1.Kind != n2.Kind {
		return false
	}
	if n1.Kind == yaml.ScalarNode {
		return n1.Value == n2.Value
	}

	if len(n1.Content) != len(n2.Content) {
		return false
	}
	for i, c1 := range n1.Content {
		c2 := n2.Content[i]
		if !Yeq(c1, c2) {
			return false
		}
	}
	return true
}

func Y2string(n *yaml.Node) string {
	out, err := yaml.Marshal(n)
	if err != nil {
		return "<error>"
	}
	return string(out)
}

var leadCommentPat = regexp.MustCompile(`^(#.*[\r\n]+)*---[\r\n]+`)

func SplitLeadComment(data []byte) (string, []byte) {
	locs := leadCommentPat.FindIndex(data)
	if locs != nil {
		return string(data[:locs[1]]), data[locs[1]:]
	}
	return "", data
}
